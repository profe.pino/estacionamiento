
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Informática
 */
public class Vehiculo {
    private String patente;
    private String marca;
    private String modelo;
    private int anno;
    private Propietario propietario;

    public Vehiculo() {
    }

    public Vehiculo(String patente, String marca, String modelo, int anno, Propietario propietario) {
        this.patente = patente;
        this.marca = marca;
        this.modelo = modelo;
        this.anno = anno;
        this.propietario = propietario;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        if (patente!=null && patente.trim().length()>0 )
            this.patente = patente;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        if (marca!=null && marca.trim().length()>0 )
            this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        if (modelo!=null && modelo.trim().length()>0 )
            this.modelo = modelo;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        if (this.anno > 2000)
            this.anno = anno;
    }

    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        if (this.propietario!=null)
            this.propietario = propietario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (this.anno != other.anno) {
            return false;
        }
        if (!Objects.equals(this.patente, other.patente)) {
            return false;
        }
        if (!Objects.equals(this.marca, other.marca)) {
            return false;
        }
        if (!Objects.equals(this.modelo, other.modelo)) {
            return false;
        }
        return true;
    }
    
    public void mostrarInformacionVehiculo() {
        System.out.println("            VEHICULO");
        System.out.println("-------------------------------");
        System.out.println(" Patente :"+this.patente);
        System.out.println(" Marca   :"+this.marca);
        System.out.println(" Modelo  :"+this.modelo);
        System.out.println(" Año     :"+this.anno);
        System.out.println("-------------------------------");
        this.propietario.mostrarInformacionPropietario();
    }
    
    
    
    
    
}
