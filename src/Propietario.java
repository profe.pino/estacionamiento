
import java.util.Date;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Informática
 */
public class Propietario {
    private String nombre;
    private String rut;
    private String telefono;
    private Date fechaNacimiento;

    public Propietario() {
    }

    public Propietario(String nombre, String rut, String telefono, Date fechaNacimiento) {
        this.nombre = nombre;
        this.rut = rut;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Propietario other = (Propietario) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.rut, other.rut)) {
            return false;
        }
       
        return true;
    }

    
    
    void mostrarInformacionPropietario() {
        System.out.println("            PROPIETARIO");
        System.out.println("-------------------------------");
        System.out.println(" Nombre  :"+this.nombre);
        System.out.println(" Rut     :"+this.rut);
        System.out.println(" Telefono:"+this.telefono);
        System.out.println(" Fecha   :"+this.fechaNacimiento);
        System.out.println("-------------------------------");
    }
    
}
